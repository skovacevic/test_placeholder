xquery version "1.0" encoding "Utf-8";

(: _input_schema= :)
(: _output_schema= :)
(: _input_element= :)
(: _output_element= :)
(: _is_complex=false :)
(: _type=request :)
(: _shared_data=:)
(: _referenced_keys=:)

declare namespace xf = "urn:aorta:hellow_world_req";



declare function xf:hellow_world_req($request as element(*))
    as element(*) {
        
            let $payload := $request//*:Payload
            return

		<AdapterMessage>
		  <payload>{data($payload)}</payload>
		  <httpauth/>
		  <method>GET</method>
		  <contentType/>
		  <converterRequest/>
		  <converterResponse/>
		  <relativeuri/>
		  <query-params/>
		</AdapterMessage>
};

declare variable $request as element(*) external;


xf:hellow_world_req($request)
