xquery version "1.0" encoding "Utf-8";

(: _input_schema=input :)
(: _output_schema= :)
(: _input_element=input :)
(: _output_element= :)
(: _is_complex=false :)
(: _type=nb-request :)
(: _shared_data=:)
(: _referenced_keys=:)

declare namespace mhs = "urn:com:aorta:pe:messageheader:v01";
declare namespace msg = "urn:com:aorta:pe:aortamessage:v01";
declare namespace ph = "urn:com:aorta:pe:processheader:v01";
declare namespace pld = "urn:com:aorta:pe:payload:v01";
declare namespace xf = "urn:aorta:request_nb";



declare function xf:request_nb($request as element(*))
    as element(*) {
        
            let $payload := $request//*:Payload
            return

		<AdapterMessage>
		  <payload>
		    <msg:AortaMessage xmlns:msg="urn:com:aorta:pe:aortamessage:v01">
		      <msg:MessageType/>
		      <msg:Version/>
		      <mhs:MessageHeader xmlns:mhs="urn:com:aorta:pe:messageheader:v01">
		        <mhs:RefId/>
		        <mhs:RequestId/>
		        <mhs:MsgId/>
		        <mhs:RefDateTime/>
		        <mhs:BusinessEvent/>
		        <mhs:JobId/>
		        <mhs:ExternalId/>
		        <mhs:UseCase/>
		        <mhs:Sender>
		          <mhs:SenderCode/>
		          <mhs:CountryCode/>
		          <mhs:Affiliate/>
		          <mhs:Instance/>
		        </mhs:Sender>
		        <mhs:ObjectReference>
		          <mhs:ObjectName>[ep_test]</mhs:ObjectName>
		          <mhs:ObjectKeyName/>
		          <mhs:ObjectKeyData/>
		          <mhs:Domain/>
		        </mhs:ObjectReference>
		        <mhs:ObjectContext/>
		      </mhs:MessageHeader>
		      <ph:ProcessHeader xmlns:ph="urn:com:aorta:pe:processheader:v01"/>
		      <pld:Payload xmlns:pld="urn:com:aorta:pe:payload:v01">{data($payload)}</pld:Payload>
		    </msg:AortaMessage>
		  </payload>
		</AdapterMessage>
};

declare variable $request as element(*) external;


xf:request_nb($request)
