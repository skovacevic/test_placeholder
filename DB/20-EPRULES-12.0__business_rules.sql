alter session set current_schema = eprules;

insert into lookup_target(EXECUTION_PLAN,ROLLBACK,ID,PROJECT_NAME,PRIORITY,SERVICE_GROUP_ID) values ('oramds:/apps/test_placeholder/executionPlans/hello_world.xml',NULL,'f39f4e85-3e3b-4866-b4ea-f6d782528dcf','test_placeholder',NULL,NULL);

merge into lookup_rule LR using dual ON (LR.name = 'hello_world' AND LR.target_id = 'f39f4e85-3e3b-4866-b4ea-f6d782528dcf' AND LR.field_xpath = 'Affiliate')
WHEN MATCHED THEN UPDATE SET value = 'SR'
WHEN NOT MATCHED THEN INSERT (ID,NAME,VALUE,TARGET_ID,PROJECT_NAME,FIELD_XPATH) values ('8987d129-6f21-41f8-bfee-de920fcb87f5','hello_world','SR','f39f4e85-3e3b-4866-b4ea-f6d782528dcf','test_placeholder','Affiliate');

merge into lookup_rule LR using dual ON (LR.name = 'hello_world' AND LR.target_id = 'f39f4e85-3e3b-4866-b4ea-f6d782528dcf' AND LR.field_xpath = 'UseCase')
WHEN MATCHED THEN UPDATE SET value = 'hello_world'
WHEN NOT MATCHED THEN INSERT (ID,NAME,VALUE,TARGET_ID,PROJECT_NAME,FIELD_XPATH) values ('88f07654-4616-4871-bc95-d987c81dd2a8','hello_world','hello_world','f39f4e85-3e3b-4866-b4ea-f6d782528dcf','test_placeholder','UseCase');

commit;